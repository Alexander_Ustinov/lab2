package com.example.lab2.coloreffect;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private final Map<String, String> colorsMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] colorTexts = getResources().getStringArray(R.array.color_texts);
        String[] colors = getResources().getStringArray(R.array.color_selection);
        for (int i = 0; i < colors.length; i++) {
            colorsMap.put(colors[i], colorTexts[i]);
        }
    }

    public void onClickColorEffect(View view) {
        Spinner spinner = findViewById(R.id.spinner);
        TextView textView = findViewById(R.id.textView);
        String color = colorsMap.get(String.valueOf(spinner.getSelectedItem()));
        textView.setText(color);
    }
}
